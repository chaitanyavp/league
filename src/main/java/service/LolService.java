package service;

import model.LolChampion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.LolRepository;

import java.util.List;

@Service
public class LolService implements LolServiceInterface{

    @Autowired
    private LolRepository lolRepository;

    @Override
    public List<LolChampion> findAll() {
        return lolRepository.findAll();
    }

    @Override
    public void save(LolChampion champ) {
        lolRepository.save(champ);
    }
}
