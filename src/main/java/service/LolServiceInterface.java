package service;

import model.LolChampion;

import java.util.List;

public interface LolServiceInterface {

    List<LolChampion> findAll();

    void save(LolChampion champ);
}
