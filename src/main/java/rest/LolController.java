package rest;

import model.LolChampion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.LolService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/league")
public class LolController {

    @Autowired
    private LolService lolService;

    @GetMapping
    public List<LolChampion> findAll(){
        return lolService.findAll();
    }
}
