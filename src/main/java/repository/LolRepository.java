package repository;

import model.LolChampion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;


@Component
public interface LolRepository extends MongoRepository<LolChampion, String> {

}
