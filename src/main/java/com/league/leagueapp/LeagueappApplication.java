package com.league.leagueapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeagueappApplication {

    public static void main(String[] args) {
        SpringApplication.run(LeagueappApplication.class, args);
    }

}
